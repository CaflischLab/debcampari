!--------------------------------------------------------------------------!
! LICENSE INFO:                                                            !
!--------------------------------------------------------------------------!
!    This file is part of CAMPARI.                                         !
!                                                                          !
!    Version 3.0                                                           !
!                                                                          !
!    Copyright (C) 2017, The CAMPARI development team (current and former  !
!                        contributors)                                     !
!                        Andreas Vitalis, Adam Steffen, Rohit Pappu, Hoang !
!                        Tran, Albert Mao, Xiaoling Wang, Jose Pulido,     !
!                        Nicholas Lyle, Nicolas Bloechliger, Marco Bacci,  !
!                        Davide Garolini, Jiri Vymetal                     !
!                                                                          !
!    Website: http://sourceforge.net/projects/campari/                     !
!                                                                          !
!    CAMPARI is free software: you can redistribute it and/or modify       !
!    it under the terms of the GNU General Public License as published by  !
!    the Free Software Foundation, either version 3 of the License, or     !
!    (at your option) any later version.                                   !
!                                                                          !
!    CAMPARI is distributed in the hope that it will be useful,            !
!    but WITHOUT ANY WARRANTY; without even the implied warranty of        !
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         !
!    GNU General Public License for more details.                          !
!                                                                          !
!    You should have received a copy of the GNU General Public License     !
!    along with CAMPARI.  If not, see <http://www.gnu.org/licenses/>.      !
!--------------------------------------------------------------------------!
! AUTHORSHIP INFO:                                                         !
!--------------------------------------------------------------------------!
!                                                                          !
! MAIN AUTHOR:  Davide Garolini                                            !
!                                                                          !
!--------------------------------------------------------------------------!
!
#include "macros.i"
!
!
module special_dis
contains
  !
  ! dgarol function adapted for direct mutual info. Inpust are INTEGERs
  !-------------------------------------------------------------------------
  subroutine dmi(v1, l1, lev1, v2, l2, lev2, ans, mps, zvalue)
      implicit none

      ! Input variables
      integer, intent(in) :: l1, l2
      integer, dimension(l1), intent(in) :: v1
      integer, dimension(l1) :: v1_tmp
      integer, dimension(l2), intent(in) :: v2
      integer, dimension(l2) :: v2_tmp
      integer, intent(in) :: lev1, lev2

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Output variables:
      ! ans = raw MI
      ! mps = jackknife bias corrected MI
      ! zvalue = z value for hypothesis that mps == 0
      RTYPE, intent(out) :: ans, mps, zvalue
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! Local variables
      integer i, j, k, l
      RTYPE :: tot

      ! Local dynamic arrays
      RTYPE, dimension(lev1,lev2) :: tab
      RTYPE, dimension(lev1,lev2) :: ptab
      RTYPE, dimension(lev1) :: rv
      RTYPE, dimension(lev2) :: cv

      ! Jackknife replication MI values
      RTYPE, dimension(l1) :: ansjk

      ! Jackknife pseudo-values
      RTYPE, dimension(l1) :: ps

      ! SD of pseudo-values
      RTYPE :: sdps

      ! Initialise dynamic arrays
      tab = 0.0
      v1_tmp = v1 + 1
      v2_tmp = v2 + 1

      ! Cross tab for joint distribution
      do i = 1, l1
          tab(v1_tmp(i), v2_tmp(i)) = tab(v1_tmp(i), v2_tmp(i)) + 1.0
      end do

      ! Marginal counts (sum over joint)
      cv = sum(tab, dim = 1)
      rv = sum(tab, dim = 2)
      tot = sum(tab)

      ! Probability tables
      ptab = tab / tot
      rv = (rv * 1.0) / tot
      cv = (cv * 1.0) / tot

      ans = 0.0

      ! Calculate MI
      do i = 1, lev1
          do j = 1, lev2
              if (ptab(i, j) > 0) then
                  ans = ans + ptab(i,j) * log(ptab(i,j) / (rv(i) * cv(j)))
              end if
          end do
      end do

      ! ! Get jackknife estimates
      ! ! Not yet modified for discrete data
      ! ansjk = 0.0
      ! do k = 1, l1
      !     ! Remove observation from the tables
      !     tab(v1_tmp(k), v2_tmp(k)) = tab(v1_tmp(k), v2_tmp(k)) - 1
      !
      !     ! Marginal counts (sum over joint)
      !     ! Some inefficiency here
      !     cv = sum(tab, dim = 1)
      !     rv = sum(tab, dim = 2)
      !     tot = sum(tab)
      !
      !     ! Probability tables
      !     ptab = tab / tot
      !     rv = rv / tot
      !     cv = cv / tot
      !
      !     do i = 1, lev1
      !         do j = 1, lev2
      !             if (ptab(i, j) > 0) then
      !                 ansjk(k) = ansjk(k) + ptab(i,j) * log(ptab(i,j) / (rv(i) * cv(j)))
      !             end if
      !         end do
      !     end do
      !
      !     ! Put observation back in table
      !     tab(v1_tmp(k), v2_tmp(k)) = tab(v1_tmp(k), v2_tmp(k)) + 1
      ! end do
      !
      mps = 0
      zvalue = 0.0
      ! ! Get bias corrected MI value and z-value from the jackknife
      ! ! using Tukey's pseudo-value approach.
      ! ! (There are probably more efficient ways to do this.)
      ! ps = real(l1, 8) * ans - (real(l1, 8) - 1.0) * ansjk
      ! mps = sum(ps) / real(l1, 8)
      ! sdps = sqrt(sum((ps - mps) * (ps - mps)) / (real(l1, 8) - 1.0))
      ! if(sdps .gt. 0.001) zvalue = sqrt(real(l1, 8)) * mps / sdps
  end subroutine


  ! mi distance: H(x,y) - MI(x,y) = H(y|x) - H(x|y)
  subroutine var_ent(v1, l1, v2, l2, ans)
      implicit none

      ! Input variables
      integer, intent(in) :: l1, l2
      integer, dimension(l1), intent(in) :: v1
      integer, dimension(l2), intent(in) :: v2

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Output variables:
      ! ans = raw MI
      RTYPE, intent(out) :: ans
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! Local variables
      integer i, j
      RTYPE :: tot

      ! Local dynamic arrays
      RTYPE, dimension(l1,l2) :: tab
      RTYPE, dimension(l1,l2) :: ptab
      RTYPE, dimension(l1) :: rv
      RTYPE, dimension(l2) :: cv

      ! Initialise dynamic arrays
      tab = 0.0

      ! Cross tab for joint distribution
      do i = 1, l1
          do j = 1, l2
              tab(i, j) =  v1(i) + v2(j)
          end do
      end do

      ! Marginal counts (sum over joint)
      cv = sum(tab, dim = 1)
      rv = sum(tab, dim = 2)
      tot = sum(tab)

      ! Probability tables
      ptab = tab / tot
      rv = (rv * 1.0) / tot ! p(x)
      cv = (cv * 1.0) / tot ! p(y)

      ans = 0.0

      ! Calculate MI
      do i = 1, l1
          do j = 1, l2
              if (ptab(i, j) > 0) then
                  ans = ans + &
                  & ptab(i,j) * log(ptab(i,j) / (rv(i))) + &! p(y\x)
                  & ptab(i,j) * log(ptab(i,j) / (cv(j))) ! p(x\y)
              end if
          end do
      end do

  end subroutine

  !-------------------------------------------------------------------------------
end module special_dis
