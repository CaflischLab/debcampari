[![pipeline status](https://gitlab.com/CaflischLab/debcampari/badges/master/pipeline.svg)](https://gitlab.com/CaflischLab/debcampari/commits/master)
[![coverage report](https://gitlab.com/CaflischLab/debcampari/badges/master/coverage.svg)](https://gitlab.com/CaflischLab/debcampari/commits/master)


# Welcome to CAMPARI software
This is a simple mirror of the original software that is on [SourceForge](http://campari.sourceforge.net/V3/index.html). 


Please refer to this link for citations and correct autorship.