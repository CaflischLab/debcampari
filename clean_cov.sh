#!/bin/bash
# setting of the source directory
DIR=$1
if test "${DIR}" = "" -o -z "${DIR}"; then
  DIR=`pwd`
fi

echo "cleaning up..."

for i in ${DIR}/*gcov ; do if [ -e ${i} ]; then rm ${i}; fi; done

echo "...done"
