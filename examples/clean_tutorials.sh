#!/bin/bash

RM='rm'


if [ -z $1]; then
	FOL="."
else
	FOL=$1
fi


for i in ${FOL}/*.int; do if [ -e $i ]; then echo "removing" $i; ${RM} $i; fi; done
for i in ${FOL}/*.dat; do if [ -e $i ]; then echo "removing" $i; ${RM} $i; fi; done
for i in ${FOL}/*.pdb; do if [ -e $i ]; then echo "removing" $i; ${RM} $i; fi; done
for i in ${FOL}/*.vmd; do if [ -e $i ]; then echo "removing" $i; ${RM} $i; fi; done
#for i in ${1}/*.in; do if [ -e $${i} ]; then ${RM} $${i}; fi; done # this is probably already in the tutorial (that is why I used these tutorials)
#for i in ${1}/*.key; do if [ -e $${i} ]; then ${RM} $${i}; fi; done # similarly as .in


